using Discord;
using Discord.Commands;
using System.Threading.Tasks;

namespace Your.Namespace.Here 
{
  public class ExampleCommand : ModuleBase<SocketCommandContext>
  {
    [Command("Example")]
    public async Task Example() 
    {
      await Context.Message.AddReactionAsync(new Emoji(RawEmoji.QUESTION));
      // adds a ❓ emoji to your message
    }
  }
}